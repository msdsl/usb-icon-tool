欢迎使用本软件
本软件没有版权，可以任意修改
1.1版本更新日志：
新增效果预览功能
重绘帮助窗口效果
新增文件转换器
如果您对软件有任何意见，请用邮箱反馈
我的邮箱：Goldbaby2022@163.com
希望您满意 :D
﻿
Welcome to use this software
This software does not have copyright and can be modified arbitrarily
1.1 Version update log:
New effect preview function added
Redraw Help Window Effects
Add file converter
If you have any comments on the software, please provide feedback via email
My email: Goldbaby2022@163.com
Hope you are satisfied: D


本ソフトウェアへようこそ
本ソフトウェアには著作権がありませんので、任意に変更することができます
1.1バージョン更新ログ：
新しい効果プレビュー機能
ヘルプウィンドウ効果の再描画
新しいファイルコンバータ
ソフトウェアに関するご意見があれば、メールでフィードバックしてください
私のメールアドレス：Goldbaby2022@163.com
ご満足いただけますように：D


소프트웨어 시작
본 소프트웨어는 저작권이 없으므로 임의로 수정할 수 있습니다.
1.1 버전 업데이트 로그:
새로운 효과 미리 보기 기능
도움말 창 효과 다시 그리기
새 파일 변환기
소프트웨어에 대한 의견이 있으시면 메일박스 피드백을 사용하십시오.
내 메일박스:Goldbaby2022@163.com
마음에 드셨으면 좋겠습니다:D


歡迎使用本軟件
本軟件沒有版權，可以任意修改
1.1版本更新日誌：
新增效果預覽功能
重繪幫助視窗效果
新增檔案轉換器
如果您對軟件有任何意見，請用郵箱迴響
我的郵箱： Goldbaby2022@163.com
希望您滿意：D


欢迎用本幽。
幽事无版权所有，可得任意改。
一分十分一本新日志：
新增效观大义，
重绘佐窗口之功；
新法移文更易器。
若以君系之，请于铺箱递。
祝曰：Goldbaby2022@163.com
愿王之说曰:D

Bienvenue dans ce logiciel
Ce logiciel n'a pas de droits d'auteur et peut être modifié à volonté
Journal des mises à jour de version 1.1:
Ajout de la fonction de prévisualisation des effets
Redessiner l'effet de la fenêtre d'aide
Ajout d'un convertisseur de fichiers
Si vous avez des commentaires sur le logiciel, s'il vous plaît Feedback par boîte aux lettres
Ma boîte mail: Goldbaby2022@163.com
En espérant votre satisfaction: D
